import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import Callback from "../views/Callback.vue";
import NotAuthorized from "../views/NotAuthorized.vue";
import Main from "../views/Main.vue";


const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/Callback",
    name: "Callback",
    component: Callback
  },

  {
    path: "/NotAuthorized",
    name: "NotAuthorized",
    component: NotAuthorized
  },

  {
    path: "/Main",
    name: "Main",
    component: Main
  },


];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
