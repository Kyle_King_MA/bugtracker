import auth0 from 'auth0-js';


const WebAuth = new auth0.WebAuth({
    domain: "dev-ohc8ny0o.us.auth0.com",
    clientId: "pysMLZoD42hqSaDVcTemoG5CBF0tkT6j",
    redirectUri: '',
    responseType: 'token id_token',
    scope: 'open profile',
});

const loginsys = () => {
    WebAuth.authorize();
};

export { loginsys };


