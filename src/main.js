import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAXoOZ0b17tEKgwuMwRFKElC3x50kc334o",
    authDomain: "bugtracker-d4add.firebaseapp.com",
    projectId: "bugtracker-d4add",
    storageBucket: "bugtracker-d4add.appspot.com",
    messagingSenderId: "893806843186",
    appId: "1:893806843186:web:8f759237955d916a7f332b",
    measurementId: "G-KWZG76SM8D"
};

firebase.initializeApp(firebaseConfig);

createApp(App).use(store).use(router).mount("#app");
