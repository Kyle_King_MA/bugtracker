const express = require('express');
const mysql = require('mysql');

const app = express();

let connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "practice",
    port: "3306"
})

connection.connect((err) => {
    if(err){
        throw err
    }else{
        console.log("connected")
    }
})

const port = process.env.PORT || 5000;
app.listen(port);
